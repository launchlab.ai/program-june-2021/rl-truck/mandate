# Calculates values from the suspension

from tools import *
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Steering_Input = create_input(extension, "Steering Input", Vortex.Types.Type_VxReal)
    extension.Max_Rack_Travel = create_input(extension, 'Max Rack Travel', Vortex.Types.Type_VxReal)

    # Internal inputs

    # Parameters

    # ICD outputs

    # Internal outputs
    extension.Rack_Position = create_output(extension, "Rack Position", Vortex.Types.Type_VxReal)
    extension.Rack_Limit_Positive = create_output(extension, 'Rack Limit Positive', Vortex.Types.Type_VxReal)
    extension.Rack_Limit_Negative = create_output(extension, 'Rack Limit Negative', Vortex.Types.Type_VxReal)


    # Descriptions
    extension.Steering_Input.setDescription("Steering input signal from 0 to 1.")
    extension.Max_Rack_Travel.setDescription("Travel limit of rack constriant in m. Rack will stop this distance in either direction")
    extension.Rack_Position.setDescription("Position to set in rack constraint in m")
    extension.Rack_Limit_Positive.setDescription("Positive travel limit of rack constriant in m")
    extension.Rack_Limit_Negative.setDescription("Negative travel limit of rack constriant in m")


def pre_step(extension):

    extension.Rack_Position.value = extension.Steering_Input.value * extension.Max_Rack_Travel.value

    extension.Rack_Limit_Positive.value = extension.Max_Rack_Travel.value
    extension.Rack_Limit_Negative.value = -extension.Max_Rack_Travel.value
