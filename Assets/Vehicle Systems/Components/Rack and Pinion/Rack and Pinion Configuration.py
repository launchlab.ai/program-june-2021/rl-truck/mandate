# This script uses a set of control point inputs and calculates part and 
# constraint positions accordingly

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    create_input(extension, 'Enable', Vortex.Types.Type_Bool).setDescription("True to update calculations")

    create_parameter(extension, 'Rack End Position', Vortex.Types.Type_VxVector3).setDescription("Position of the joint between the rack end and tie rod")
    create_parameter(extension, 'Tie Rod Outer Position', Vortex.Types.Type_VxVector3).setDescription("Position of the joint between the tie rod and knuckle steering arm")

    # Part transforms
    create_output(extension, 'Rack Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Tie Rod L Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Tie Rod R Transform', Vortex.Types.Type_VxMatrix44)

    # Constraint attachments
    create_output(extension, 'Tie Rod Outer L Offset', Vortex.Types.Type_VxVector3)
    create_output(extension, 'Tie Rod Outer R Offset', Vortex.Types.Type_VxVector3)

def paused_update(extension):
    config_update(extension)

def config_update(extension):
    # Skip update if disabled
    if not extension.inputs.Enable.value:
        return

    # Positions of points
    pin_pos = extension.parameters.Rack_End_Position.value
    tr_pos = extension.parameters.Tie_Rod_Outer_Position.value
    # Set y positive in case user chose point on negative side
    pin_pos.y = abs(pin_pos.y)
    tr_pos.y = abs(tr_pos.y)

    # Part transforms
    extension.outputs.Rack_Transform.value = Vortex.createTranslation(pin_pos.x, 0.0, pin_pos.z)
    extension.outputs.Tie_Rod_L_Transform.value = Vortex.createTranslation(pin_pos)
    extension.outputs.Tie_Rod_R_Transform.value = Vortex.createTranslation(pin_pos.x, -pin_pos.y, pin_pos.z)

    # Constraint attachments
    tr_offset = tr_pos - pin_pos
    extension.outputs.Tie_Rod_Outer_L_Offset.value = tr_offset # Outer position is offset from part origin, which is inner position
    tr_offset.y = -tr_offset.y
    extension.outputs.Tie_Rod_Outer_R_Offset.value = tr_offset
