# This script enables an output when an extension is present

def paused_update(extension):
    extension.outputs.Enable.value = extension.parameters.Extension.value is not None
