# Controls the gear ratio to behave as an automatic transmission. Gear ratios are specified, shift up 
# and down is done based on current RPM

from tools import *
import math
import csv
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Gear_Selection = create_input(extension, 'Gear Selection', Vortex.Types.Type_Int)
    extension.Min_Gear_Selection = create_input(extension, 'Min Gear Selection', Vortex.Types.Type_Int)
    extension.Park = create_input(extension, 'Park', Vortex.Types.Type_Bool)
    extension.Transition_Time = create_input(extension, 'Transition Time', Vortex.Types.Type_VxReal, 0.5)
    extension.Shift_Again_Delay = create_input(extension, 'Shift Again Delay', Vortex.Types.Type_VxReal, 1.0)
    extension.Low_Shift_Throttle = create_input(extension, 'Low Shift Throttle', Vortex.Types.Type_VxReal, 0.5)
    extension.High_Shift_Throttle = create_input(extension, 'High Shift Throttle', Vortex.Types.Type_VxReal, 0.9)
    extension.Low_Shift_Down = create_input(extension, 'Low Shift Down', Vortex.Types.Type_VxReal, 0.3)
    extension.Low_Shift_Up = create_input(extension, 'Low Shift Up', Vortex.Types.Type_VxReal, 0.5)
    extension.High_Shift_Down = create_input(extension, 'High Shift Down', Vortex.Types.Type_VxReal, 0.5)
    extension.High_Shift_Up = create_input(extension, 'High Shift Up', Vortex.Types.Type_VxReal, 0.9)
    extension.Shift_Scale = create_input(extension, 'Shift Scale', Vortex.Types.Type_VxReal, 3000)
    extension.Shaft_Friction = create_input(extension, 'Shaft Friction', Vortex.Types.Type_VxReal)
    extension.Efficiency = create_input(extension, 'Efficiency', Vortex.Types.Type_VxReal, 1.0)
    extension.Shift_Efficiency = create_input(extension, 'Shift Efficiency', Vortex.Types.Type_VxReal, 0.5)

    # Internal inputs
    extension.Input_Shaft_Speed = create_input(extension, 'Input Shaft Speed', Vortex.Types.Type_VxReal)
    extension.Throttle = create_input(extension, 'Throttle', Vortex.Types.Type_VxReal)
    extension.Gear_Ratio_Torque = create_input(extension, 'Gear Ratio Torque', Vortex.Types.Type_VxReal)
    extension.Output_Shaft_Speed = create_input(extension, "Output Shaft Speed", Vortex.Types.Type_VxReal)

    # Parameters
    extension.Gear_Ratio_Table = create_parameter(extension, "Gear Ratio Table", Vortex.Types.Type_VxFilename)
    # Shift schedule is not yet implemented
    # extension.Gear_Shift_Schedule = create_parameter(extension, "Gear Shift Schedule", Vortex.Types.Type_VxFilename)

    # ICD outputs
    extension.Current_Gear = create_output(extension, 'Current Gear', Vortex.Types.Type_Int)
    extension.Gear_Ratio = create_output(extension, 'Gear Ratio', Vortex.Types.Type_VxReal)
    extension.Shift_Transition = create_output(extension, 'Shift Transition', Vortex.Types.Type_Bool)
    extension.Shift_Up_RPM = create_output(extension, 'Shift Up RPM', Vortex.Types.Type_VxReal)
    extension.Shift_Down_RPM = create_output(extension, 'Shift Down RPM', Vortex.Types.Type_VxReal)
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Gear_Constraint_Enable = create_output(extension, 'Gear Constraint Enable', Vortex.Types.Type_Bool)
    extension.Hinge_Mode = create_output(extension, 'Hinge Mode', Vortex.Types.Type_Int)
    extension.Hinge_Friction = create_output(extension, 'Hinge Friction', Vortex.Types.Type_VxReal)

    # Descriptions
    extension.Gear_Selection.setDescription("Selected gear, positive for forward, negative for reverse, zero for neutral")
    extension.Min_Gear_Selection.setDescription("If a transmission is prevented from shifting into the lowest gears, this parameter can be set to the number of the lowest allowable gear. Set 0 for full range, negative for reverse.")
    extension.Park.setDescription("True to enable parking mode, locking transmission shaft")
    extension.Transition_Time.setDescription("Transition time during shift between previous gear and next gear")
    extension.Shift_Again_Delay.setDescription("After a gear change has occurred, automatic shifting will be prevented until this amount of time has passed")
    extension.Low_Shift_Throttle.setDescription("When current Throttle is at or below this value, Low Shifting values will be used. Between Low and High Shift Throttle, value will be linearly interpolated")
    extension.High_Shift_Throttle.setDescription("When current Throttle is at or above this value, Low Shifting values will be used. Between Low and High Shift Throttle, value will be linearly interpolated")
    extension.Low_Shift_Up.setDescription("Point to shift up at low shifting throttle position. Shift will happen when input shaft RPM > Shift Up * Shift Scale")
    extension.Low_Shift_Down.setDescription("Point to shift down at low shifting throttle position. Shift will happen when input shaft RPM < Shift Down * Shift Scale")
    extension.High_Shift_Up.setDescription("Point to shift up at high shifting throttle position. Shift will happen when input shaft RPM > Shift Up * Shift Scale")
    extension.High_Shift_Down.setDescription("Point to shift down at high shifting throttle position. Shift will happen when input shaft RPM < Shift Down * Shift Scale")
    extension.Shift_Scale.setDescription("Scale that multiplies the shift points. If this is 1, shift points correspond to RPM of engine. If this is set to the max RPM of th engine, shift points correspond to fraction of max RPM.")
    extension.Shaft_Friction.setDescription("Add friction on the output shaft of the transmission")
    extension.Efficiency.setDescription("Efficiency of coupling. Friction is applied to output shaft proportional to the power transmitted through the transmission")
    extension.Shift_Efficiency.setDescription("Efficiency is often lower while shifting gears, so during Transition Time, Shift Efficiency determines friction on output shaft")

    extension.Input_Shaft_Speed.setDescription("Speed of shaft feeding into transmission")
    extension.Throttle.setDescription("Current throttle position")
    extension.Gear_Ratio_Torque.setDescription("Torque of gear ratio constraint")
    extension.Output_Shaft_Speed.setDescription("Speed of the output shaft from the hinge of this component")

    extension.Gear_Ratio_Table.setDescription(".csv file containing gear ratios of the transmission. Starting with negative values for reverse gears, then zero for neutral, and positive values for forward gears")
    # extension.Gear_Shift_Schedule.setDescription(".csv file containing a table of shifting points for each gear. If left blank, Low and High shift RPM parameters will be used instead")

    extension.Current_Gear.setDescription("Index number of current gear, positive for forward, negative for reverse, zero for neutral")
    extension.Gear_Ratio.setDescription("Ratio of current gear")
    extension.Shift_Transition.setDescription("True during shift transition")
    extension.Shift_Up_RPM.setDescription("Shift Up RPM based on current throttle position")
    extension.Shift_Down_RPM.setDescription("Shift Down RPM based on current throttle position")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")

    extension.Gear_Constraint_Enable.setDescription("Enable gear ratio constraint. Disabled when in Neutral")
    extension.Hinge_Mode.setDescription("Mode of hinge constraint")
    extension.Hinge_Friction.setDescription("Friction of hinge constraint")

    # Load gear ratios and put in a dictionary
    try:
        with open(extension.Gear_Ratio_Table.value, 'r') as f:
            reader = csv.reader(f)
            your_list = list(reader)
    except Exception as e: 
        print(e)
        extension.gear_ratios = None
        extension.max_gear = 0
        extension.min_gear = 0
        raise ValueError("Unable to load Gear Ratio Table")
            
    index = [int(x) for x in your_list[0]]
    gear = [float(x) for x in your_list[1]]
    extension.gear_ratios = dict(zip(index, gear))
    extension.max_gear = max(index)
    extension.min_gear = min(index)


    # Current gear
    extension.gear = 1
    # Shift-again timer that counts down during shift - Must be larger than transition time
    extension.shift_delay = Timer(max(extension.Shift_Again_Delay.value, extension.Transition_Time.value),
                            extension.getApplicationContext().getSimulationTimeStep())
    # Shift transition timer that counts down as gear transition to new ratio
    extension.transition = Timer(extension.Transition_Time.value, extension.getApplicationContext().getSimulationTimeStep())
    # Amount that gear ratio can be changed each time step during transition - updated when shifted
    extension.ratio_rate = 0.0


def pre_step(extension):

    # If the shift table is blank, define shift RPM based on inputs
    try:
        shift_throttle = [0.0, extension.Low_Shift_Throttle.value, extension.High_Shift_Throttle.value, 1.0]

        scale = extension.Shift_Scale.value
        extension.up_shift = LinearInterpolation(shift_throttle, 
            [extension.Low_Shift_Up.value * scale, extension.Low_Shift_Up.value * scale, 
             extension.High_Shift_Up.value * scale, extension.High_Shift_Up.value * scale])
        extension.down_shift = LinearInterpolation(shift_throttle,
            [extension.Low_Shift_Down.value * scale, extension.Low_Shift_Down.value * scale, 
            extension.High_Shift_Down.value * scale, extension.High_Shift_Down.value * scale])
    except:
        extension.up_shift = None
        extension.down_shift = None
        raise ValueError("Invalid shift throttle. Must be 0.0 < Low Shift Throttle < High Shift Throttle < 1.0")


    if (extension.gear_ratios is None) or (extension.up_shift is None):
        return

    # - Shifting Behaviour ---------------------------------

    rpm = extension.Input_Shaft_Speed.value *30/math.pi
    gear_selection = clamp(extension.Gear_Selection.value, extension.min_gear, extension.max_gear)
    throttle = extension.Throttle.value

    # Minimum gear defined by inputs
    if gear_selection > 0: # for forward, min gear is between 1 and selected gear
        min_gear_selection = clamp(extension.Min_Gear_Selection.value, 1, gear_selection)
    elif gear_selection < 0: # for reverse, min gear is between selected gear and -1, but flip to positive for shifting logic
        min_gear_selection = abs(clamp(extension.Min_Gear_Selection.value, gear_selection, -1))
    else: # neutral
        min_gear_selection = 0


    if gear_selection == 0: # Neutral
        extension.gear = 0
    else:
        # Check if switched between forward and reverse, and reset to min
        # Otherwise if current gear is eg. 4, shifting to reverse will start in -4
        if (extension.gear * gear_selection) < 0:
            extension.gear = min_gear_selection

        # If shifting out of neutral, shift again delay to prevent sudden shift up if RPM is high
        if extension.gear == 0:
            extension.shift_delay.restart()

        # Shift up or down, based on current RPM
        # All based on positive gear numbers, since logic is equivalent for foward and reverse
        extension.gear = abs(extension.gear)

        extension.Shift_Up_RPM.value = extension.up_shift(throttle)
        extension.Shift_Down_RPM.value = extension.down_shift(throttle)

        if not extension.shift_delay():
            if rpm >= extension.up_shift(throttle) and extension.gear < abs(gear_selection):
                extension.gear += 1
                extension.shift_delay.restart()
                extension.transition.restart()
                # Calculate rate at which ratio can be changed each step during transition time
                extension.ratio_rate = (abs(extension.gear_ratios[extension.gear - 1] - extension.gear_ratios[extension.gear])
                                    * extension.getApplicationContext().getSimulationTimeStep() / extension.Transition_Time.value)
            elif rpm <= extension.down_shift(throttle) and extension.gear > abs(min_gear_selection):
                extension.gear -= 1
                extension.shift_delay.restart()
                extension.transition.restart()
                # Calculate rate at which ratio can be changed each step during transition time
                extension.ratio_rate = (abs(extension.gear_ratios[extension.gear - 1] - extension.gear_ratios[extension.gear])
                                    * extension.getApplicationContext().getSimulationTimeStep() / extension.Transition_Time.value)

        # Recheck the min and max gear, in case they were changed by user
        extension.gear = clamp(extension.gear, min_gear_selection, abs(gear_selection))
        # Apply sign to match selected direction
        extension.gear = int(math.copysign(extension.gear, gear_selection))

    # Update timers
    extension.shift_delay.update()
    extension.transition.update()


    # - Gear Ratios ---------------------------------

    extension.Current_Gear.value = extension.gear
    if extension.transition():
        # Calculate ratio during transition
        # 
        new_ratio = extension.gear_ratios[extension.gear]
        extension.Gear_Ratio.value = clamp(new_ratio, extension.Gear_Ratio.value - extension.ratio_rate, extension.Gear_Ratio.value + extension.ratio_rate)
    else:
        extension.Gear_Ratio.value = extension.gear_ratios[extension.gear]

    extension.Gear_Constraint_Enable.value = abs(extension.gear_ratios[extension.gear]) > 0.0001
    extension.Shift_Transition.value = extension.transition()


    # - Parking Lock ---------------------------------

    if extension.Park.value:
        extension.Hinge_Mode.value = Vortex.Constraint.kControlLocked
    else:
        extension.Hinge_Mode.value = Vortex.Constraint.kControlFree


    # - Friction and Efficiency ---------------------------------

    # Gear ratio constraint reports torque on Part 1 which is input torque
    # Constraint keeps reporting last value when constraint is disabled, so set to 0 in this case
    # Negative due to convention of constraint
    input_torque = - extension.Gear_Ratio_Torque.value if extension.Gear_Constraint_Enable.value else 0.0
    # Output torque is multiplied by gear ratio
    output_torque = input_torque * extension.Gear_Ratio.value
    # Choose efficiency: use shift efficiency if during shift transition
    efficiency = extension.Shift_Efficiency.value if extension.transition() else extension.Efficiency.value
    efficiency_friction = abs(output_torque) * (1.0 - efficiency)

    extension.Hinge_Friction.value = efficiency_friction + extension.Shaft_Friction.value

    extension.Shaft_Speed.value = extension.Output_Shaft_Speed.value
    # Field reports net torque, so subtract friction torque from output torque
    # Since friction could be + or -, must use abs for subtraction, then reapply sign.
    extension.Shaft_Torque.value = math.copysign(max(abs(output_torque) - extension.Hinge_Friction.value, 0), output_torque)
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value
