
from tools import *
import math
import Vortex

# Computes the output power of the motor shaft.

def on_simulation_start(extension):

    extension.iShaft_Speed  = create_input(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.iShaft_Torque = create_input(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.oShaft_Power  = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    extension.iShaft_Speed.setDescription("Speed of the shaft of this component")
    extension.iShaft_Torque.setDescription("Net torque on the shaft of this component in N.m")
    extension.oShaft_Power.setDescription("Net power on output shaft in W")
    

def post_step(extension):

    extension.oShaft_Power.value = extension.iShaft_Speed.value * extension.iShaft_Torque.value
