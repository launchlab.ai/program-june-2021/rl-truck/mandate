from tools import *
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Engaged_Fraction = create_input(extension, "Engaged Fraction", Vortex.Types.Type_VxReal)
    extension.A0 = create_input(extension, "A0", Vortex.Types.Type_VxReal)
    extension.A1 = create_input(extension, "A1", Vortex.Types.Type_VxReal)
    extension.A2 = create_input(extension, "A2", Vortex.Types.Type_VxReal)

    # Internal inputs
    extension.Input_Shaft_Speed = create_input(extension, "Input Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Motor_Torque = create_input(extension, "Motor Torque", Vortex.Types.Type_VxReal)

    # ICD outputs
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Constraint_Min_Torque = create_output(extension, "Constraint Min Torque", Vortex.Types.Type_VxReal)
    extension.Constraint_Max_Torque = create_output(extension, "Constraint Max Torque", Vortex.Types.Type_VxReal)
    extension.Constraint_Loss = create_output(extension, "Constraint Loss", Vortex.Types.Type_VxReal)
    extension.Constraint_Enable = create_output(extension, 'Constraint Enable', Vortex.Types.Type_Bool)

    extension.Engaged_Fraction.setDescription("Amount that the retarder is engaged from 0: disengaged to 1: fully engaged")
    extension.A0.setDescription("Resistance torque, T, is applied according to a polynomial function of the shaft rotational speed, w: T = A0 + A1*w + A2*w^2")
    extension.A1.setDescription("Resistance torque, T, is applied according to a polynomial function of the shaft rotational speed, w: T = A0 + A1*w + A2*w^2")
    extension.A2.setDescription("Resistance torque, T, is applied according to a polynomial function of the shaft rotational speed, w: T = A0 + A1*w + A2*w^2")
    extension.Input_Shaft_Speed.setDescription("Speed of input shaft, from previous component")
    extension.Motor_Torque.setDescription("Torque on motor constraint of this component")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")
    extension.Constraint_Min_Torque.setDescription("Min torque applied to motor constraint")
    extension.Constraint_Max_Torque.setDescription("Max torque applied to motor constraint")
    extension.Constraint_Loss.setDescription("Loss to be applied to motor constraint")
    extension.Constraint_Enable.setDescription("Enable constraint. Disabled when retarder is not active")


def pre_step(extension):

    w = extension.Input_Shaft_Speed.value
    torque = extension.Engaged_Fraction.value * (extension.A0.value + extension.A1.value*w + extension.A2.value*w**2)
    extension.Constraint_Enable.value = torque > 1e-6

    extension.Constraint_Max_Torque.value = torque
    extension.Constraint_Min_Torque.value = -torque
    extension.Constraint_Loss.value = 1e-5

    extension.Shaft_Speed.value = w
    extension.Shaft_Torque.value = - extension.Motor_Torque.value if extension.Constraint_Enable.value else 0.0
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value
