# Calculates some useful values from wheel speed and torque

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Radius = create_input(extension, 'Radius', Vortex.Types.Type_VxReal, 0.5)
    extension.Braking_Torque = create_input(extension, 'Braking Torque', Vortex.Types.Type_VxReal, 0.0)

    # Internal inputs
    extension.Gear_Ratio_Torque = create_input(extension, 'Gear Ratio Torque', Vortex.Types.Type_VxReal)
    extension.Output_Shaft_Speed = create_input(extension, 'Output Shaft Speed', Vortex.Types.Type_VxReal)

    # Parameters

    # ICD outputs
    extension.Wheel_Speed = create_output(extension, 'Wheel Speed', Vortex.Types.Type_VxReal)
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Braking_Torque_Positive = create_output(extension, "Braking Torque Positive", Vortex.Types.Type_VxReal)
    extension.Braking_Torque_Negative = create_output(extension, "Braking Torque Negative", Vortex.Types.Type_VxReal)

    # Descriptions
    extension.Radius.setDescription("Radius of cylinder representing the wheel")
    extension.Gear_Ratio_Torque.setDescription("Torque of gear ratio constraint")
    extension.Output_Shaft_Speed.setDescription("Speed of the output shaft from the hinge of this component")

    extension.Wheel_Speed.setDescription("Effective linear speed of wheel")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")


def pre_step(extension):

    extension.Braking_Torque_Positive.value = extension.Braking_Torque.value
    extension.Braking_Torque_Negative.value = -extension.Braking_Torque.value

    # - Wheel Speed and Torque ---------------------------------

    # Calculate speeds of wheel
    # Car Wheel constraint velocity is negative for forward rolling
    extension.Wheel_Speed.value = - extension.Output_Shaft_Speed.value * extension.Radius.value
    extension.Shaft_Speed.value = extension.Output_Shaft_Speed.value
    # Field reports net torque, so subtract friction torque from output torque
    # Since friction could be + or -, must use abs for subtraction, then reapply sign.
    extension.Shaft_Torque.value = extension.Gear_Ratio_Torque.value
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value
