# This script sets the mass, moment of inertia, and center of mass of the specified part

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    # Inputs
    create_input(extension, 'Mass', Vortex.Types.Type_VxReal, 1.0)
    create_input(extension, 'Centre of Mass', Vortex.Types.Type_VxVector3, Vortex.VxVector3(0.0, 0.0, 0.0))
    create_input(extension, 'Inertia', Vortex.Types.Type_VxVector3, Vortex.VxVector3(0.1, 0.1, 0.1))

    # Parameters
    create_parameter(extension, 'Chassis Part', Vortex.Types.Type_Part)

    # Descriptions
    extension.inputs.Mass.setDescription("Mass of chassis")
    extension.inputs.Centre_of_Mass.setDescription("Vector of centre of mass offset")
    extension.inputs.Inertia.setDescription("Components of diagonal inertia matrix")

def paused_update(extension):
    set_mass(extension)

def set_mass(extension):

    properties = extension.parameters.Chassis_Part.value.parameterMassPropertiesContainer
    properties.mass.value = max(extension.inputs.Mass.value, 0.000001)

    properties.centerOfMassOffset.value = extension.inputs.Centre_of_Mass.value

    moi_x = max(extension.inputs.Inertia.value.x, 0.000001)
    moi_y = max(extension.inputs.Inertia.value.y, 0.000001)
    moi_z = max(extension.inputs.Inertia.value.z, 0.000001)
    properties.inertiaTensor.value = Vortex.Matrix44(moi_x, 0, 0, 0, 
                                                     0, moi_y, 0, 0, 
                                                     0, 0, moi_z, 0, 
                                                     0, 0, 0, 0)

