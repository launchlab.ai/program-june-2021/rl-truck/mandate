# This script models a hydrodynamic torque converter, based hydrodynamic theory

from tools import *
import math
import csv
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    extension.Coupling_Torque_Scale = create_input(extension, "Coupling Torque Scale", Vortex.Types.Type_VxReal, 1)
    extension.Coupling_Torque_Added = create_input(extension, "Coupling Torque Added", Vortex.Types.Type_VxReal, 0)
    extension.Stall_RPM = create_input(extension, "Stall RPM", Vortex.Types.Type_VxReal, 1000)
    extension.Stall_Torque = create_input(extension, "Stall Torque", Vortex.Types.Type_VxReal, 100)
    extension.Torque_Multiplication = create_input(extension, "Torque Multiplication", Vortex.Types.Type_VxReal, 1)
    extension.Slippage = create_input(extension, "Slippage", Vortex.Types.Type_VxReal, 0)

    # Internal inputs
    extension.Input_Shaft_Speed = create_input(extension, "Input Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Output_Shaft_Speed = create_input(extension, "Output Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Gear_Ratio_Torque = create_input(extension, "Gear Ratio Torque", Vortex.Types.Type_VxReal)

    # ICD outputs
    extension.Speed_Ratio = create_output(extension, "Speed Ratio", Vortex.Types.Type_VxReal)
    extension.Coupling_Torque = create_output(extension, "Coupling Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Speed = create_output(extension, "Shaft Speed", Vortex.Types.Type_VxReal)
    extension.Shaft_Torque = create_output(extension, "Shaft Torque", Vortex.Types.Type_VxReal)
    extension.Shaft_Power = create_output(extension, "Shaft Power", Vortex.Types.Type_VxReal)

    # Internal outputs
    extension.Coupling_Torque_Positive = create_output(extension, "Coupling Torque Positive", Vortex.Types.Type_VxReal)
    extension.Coupling_Torque_Negative = create_output(extension, "Coupling Torque Negative", Vortex.Types.Type_VxReal)
    extension.Constraint_Loss = create_output(extension, "Constraint Loss", Vortex.Types.Type_VxReal)
    extension.Gear_Ratio = create_output(extension, "Gear Ratio", Vortex.Types.Type_VxReal, 1.0)


    extension.Coupling_Torque_Scale.setDescription("Multiplies the coupling torque calculated from the torque converter model")
    extension.Coupling_Torque_Added.setDescription("Torque that is directly added to the coupling torque. This can be used to simulate a locking mechanism. A negative torque can be used to reduce the overall coupling")
    extension.Stall_RPM.setDescription("Stall RPM")
    extension.Stall_Torque.setDescription("Stall torque")
    extension.Torque_Multiplication.setDescription("Torque multiplication factor at 0 speed ratio")
    extension.Slippage.setDescription("Amount of slippage to allow when speed ratio is near 1 and coupling torque is near stall torque. A value of 0.05 corresponds to 5% slip, which allows the speed ratio to reach about 0.95. Can be set to 0 simulate the behaviour of a lockup mechanism")
    extension.Input_Shaft_Speed.setDescription("Speed of input shaft, from previous component")
    extension.Output_Shaft_Speed.setDescription("Speed of the output shaft from the hinge of this component")
    extension.Gear_Ratio_Torque.setDescription("Torque on gear ratio constraint of this component")
    extension.Speed_Ratio.setDescription("Speed ratio, output shaft / input shaft")
    extension.Coupling_Torque.setDescription("Torque applied to coupling constraint")
    extension.Shaft_Speed.setDescription("Speed of output shaft in rad/s")
    extension.Shaft_Torque.setDescription("Net torque on output shaft in N.m")
    extension.Shaft_Power.setDescription("Net power on output shaft in W")
    extension.Coupling_Torque_Positive.setDescription("Positive of torque to be applied to coupling constraint")
    extension.Coupling_Torque_Negative.setDescription("Negative of torque to be applied to coupling constraint")
    extension.Constraint_Loss.setDescription("Loss to set in gear ratio constraint, based on slippage")
    extension.Gear_Ratio.setDescription("Ratio of gear ratio constraint")

def pre_step(extension):

    # Calculate drag coefficient that corresponds to stall RPM and torque
    if extension.Stall_Torque.value <= 0 or extension.Stall_RPM.value <= 0:
        extension.drag_coeff = 0
        raise ValueError("Stall Torque or Stall RPM invalid")
    else:        
        extension.drag_coeff = extension.Stall_Torque.value / (extension.Stall_RPM.value * math.pi/30.0)**2

    # Torque multiplier is max at 0 speed ratio, reduces to 0 at 0.8 speed ratio
    extension.mult_table = LinearInterpolation([0, 0.8, 1], 
                                         [extension.Torque_Multiplication.value, 1, 1])

    # Calculate input shaft speed in rad/s, avoid negative rotation or infinite value when used in denominator
    input_speed = max(extension.Input_Shaft_Speed.value, 0.01)
    # Calculate speed ratio, cap at 10 since it's meaningless when high anyway
    sr = min(max(extension.Output_Shaft_Speed.value / input_speed, 0), 10.0)

    # Calculate coupling force from standard TC model, plus lock torque
    coupling = max(input_speed**2 * extension.drag_coeff * extension.Coupling_Torque_Scale.value + extension.Coupling_Torque_Added.value, 1.0)

    gear_ratio = max(extension.mult_table(sr), 1.0) #gear ratio for torque multiplication

    mult_torque = extension.Coupling_Torque.value * max(extension.mult_table(sr) - 1.0, 0.0)

    extension.Speed_Ratio.value = sr

    extension.Coupling_Torque.value = - extension.Gear_Ratio_Torque.value
    extension.Shaft_Speed.value = extension.Output_Shaft_Speed.value
    extension.Shaft_Torque.value = extension.Coupling_Torque.value * gear_ratio
    extension.Shaft_Power.value = extension.Shaft_Speed.value * extension.Shaft_Torque.value

    extension.Coupling_Torque_Positive.value = coupling
    extension.Coupling_Torque_Negative.value = - coupling
    extension.Gear_Ratio.value = gear_ratio

    # Set loss in the gear ratio. This is calculated to provide the specified slippage when the output shaft speed is near
    # the input shaft speed, and engine is producing torque similar to stall torque.
    # Set a lower bound on loss to avoid instability in the constraint.
    extension.Constraint_Loss.value = max(extension.Slippage.value * extension.Stall_RPM.value * math.pi/30.0 / extension.Stall_Torque.value, 1e-5)
