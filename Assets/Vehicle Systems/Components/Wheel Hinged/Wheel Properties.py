# Sets some parameters to the wheel

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    # ICD inputs
    create_input(extension, 'Radius', Vortex.Types.Type_VxReal, 0.5)
    create_input(extension, 'Width', Vortex.Types.Type_VxReal, 0.2)
    create_input(extension, 'Mass', Vortex.Types.Type_VxReal, 1.0)
    create_input(extension, 'Override Inertia', Vortex.Types.Type_Bool, False)
    create_input(extension, 'Axis Inertia', Vortex.Types.Type_VxReal, 1.0)
    create_input(extension, 'Off Axis Inertia', Vortex.Types.Type_VxReal, 0.7)

    # Parameters
    create_parameter(extension, 'Wheel Part', Vortex.Types.Type_Part)
    create_parameter(extension, 'Wheel Cylinder', Vortex.Types.Type_CollisionGeometry)


    # Descriptions

    extension.inputs.Radius.setDescription("Radius of cylinder representing the wheel")
    extension.inputs.Width.setDescription("Width of cylinder representing the wheel")
    extension.inputs.Mass.setDescription("Mass of wheel")
    extension.inputs.Override_Inertia.setDescription("Normally wheel moment of inertia is calculated automatically from mass and geometry. If Override is true, it is instead manually defined from Axis Inertia and Off Axis Inertia")
    extension.inputs.Axis_Inertia.setDescription("The inertia along the axle of the wheel. This inertia is used only if Auto Compute Inertia is false.")
    extension.inputs.Off_Axis_Inertia.setDescription("The inertia along the two axes orthogonal to the axle. Those inertias are used only if Auto Compute Inertia is false.")

def paused_update(extension):
    set_mass(extension)

def pre_step(extension):
    set_mass(extension)

def set_mass(extension):

    # - Collision Geometry ---------------------------------

    extension.parameters.Wheel_Cylinder.value.parameterRadius.value = extension.inputs.Radius.value
    extension.parameters.Wheel_Cylinder.value.parameterHeight.value = extension.inputs.Width.value

    # - Mass Properties ---------------------------------

    properties = extension.parameters.Wheel_Part.value.parameterMassPropertiesContainer
    properties.mass.value = max(extension.inputs.Mass.value, 0.000001)

    if extension.inputs.Override_Inertia.value:
        axis_moi = max(extension.inputs.Axis_Inertia.value, 0.000001)
        of_axis_moi = max(extension.inputs.Off_Axis_Inertia.value, 0.000001)
    else:
        axis_moi = 0.5 * extension.inputs.Mass.value * extension.inputs.Radius.value**2
        of_axis_moi = 1.0/12.0 * extension.inputs.Mass.value * (3 * extension.inputs.Radius.value**2 + extension.inputs.Width.value**2)

    properties.inertiaTensor.value = Vortex.Matrix44(of_axis_moi, 0, 0, 0, 
                                                     0, axis_moi, 0, 0, 
                                                     0, 0, of_axis_moi, 0, 
                                                     0, 0, 0, 0)

