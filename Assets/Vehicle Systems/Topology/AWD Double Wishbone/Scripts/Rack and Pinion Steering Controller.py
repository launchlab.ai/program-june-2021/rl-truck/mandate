from tools import *
from math import *
import Vortex

def on_simulation_start(extension):

    extension.Steering_Input = create_input(extension, "Steering Input", Vortex.Types.Type_VxReal)
    extension.Max_Rack_Position = create_input(extension, "Max Rack Position", Vortex.Types.Type_VxReal)

    extension.Rack_Position = create_output(extension, "Rack Position", Vortex.Types.Type_VxReal)


def pre_step(extension):

    extension.outputs.Rack_Position.value = extension.inputs.Steering_Input.value * extension.inputs.Max_Rack_Position.value
