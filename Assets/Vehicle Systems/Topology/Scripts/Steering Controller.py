from tools import *
from math import *
import Vortex

def on_simulation_start(extension):

    extension.Steering_Input = create_input(extension, "Steering Input", Vortex.Types.Type_VxReal)
    extension.Front_Axle_Position = create_input(extension, "Front Axle Position", Vortex.Types.Type_VxReal)
    extension.Rear_Axle_Position = create_input(extension, "Rear Axle Position", Vortex.Types.Type_VxReal)
    extension.Width = create_input(extension, "Width", Vortex.Types.Type_VxReal)
    extension.Max_Angle = create_input(extension, "Max Angle", Vortex.Types.Type_VxReal)

    extension.Angle_L = create_output(extension, "Angle L", Vortex.Types.Type_VxReal)
    extension.Angle_R = create_output(extension, "Angle R", Vortex.Types.Type_VxReal)


def pre_step(extension):

    steeringInput = clamp(extension.Steering_Input.value, -1.0, 1.0)
    angle = steeringInput * extension.Max_Angle.value
    pos_long = extension.Front_Axle_Position.value - extension.Rear_Axle_Position.value
    pos_lat = extension.Width.value / 2.0


    extension.outputs.Angle_L.value = compute_angle(pos_long, pos_lat, angle)
    extension.outputs.Angle_R.value = compute_angle(pos_long, -pos_lat, angle)

def compute_angle(pos_long, pos_lat, angle):

    outputAngle = 0.0

    tanAngle = tan(angle)
    if( abs(tanAngle) > 0.0000001 ):
        radius = pos_long / tanAngle
        divider = radius + pos_lat
        if( abs(divider) > 0.0000001 ):
            outputAngle = atan(pos_long/divider)
    return outputAngle;
