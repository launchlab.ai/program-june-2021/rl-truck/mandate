# This script is calculates the front and rear braking torques based on max torque and bias

from tools import *
import Vortex

def on_simulation_start(extension):

    extension.Brake_Input = create_input(extension, "Brake Input", Vortex.Types.Type_VxReal, 0)
    extension.Max_Torque = create_input(extension, "Max Torque", Vortex.Types.Type_VxReal, 1000)
    extension.Front_Rear_Bias = create_input(extension, "Front Rear Bias", Vortex.Types.Type_VxReal, 0.6)

    extension.Braking_Torque_F = create_output(extension, "Braking Torque F", Vortex.Types.Type_VxReal)
    extension.Braking_Torque_R = create_output(extension, "Braking Torque R", Vortex.Types.Type_VxReal)
    
def pre_step(extension):
    extension.Braking_Torque_F.value = extension.Max_Torque.value * extension.Brake_Input.value
    extension.Braking_Torque_R.value = (extension.Max_Torque.value * extension.Brake_Input.value 
                                   * (1 - extension.Front_Rear_Bias.value) / extension.Front_Rear_Bias.value)
